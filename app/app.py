from flask import Flask, render_template, request
import pandas as pd
import numpy as np
from joblib import load

app = Flask(__name__)


@app.route('/')
def index():
   return render_template("index.html")

@app.route('/model', methods=['POST'])
def model():
    input_data = request.form.get('input_data')
    input_data = input_data.replace('[','').replace(']','')
    input_data = [int(x) for x in input_data.split(',')]
    
    model = load(r"..\models\decisionTree.joblib")
    model_predict = model.predict([input_data])
    model_predict_str = str(model_predict)
    return model_predict_str


if __name__ == "__main__":
 app.run()